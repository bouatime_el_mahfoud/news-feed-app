
import { Tag } from './tag.model';
export class Article{
	//the article model
	constructor(public objectId:string,
  public name:string,
  public domain:string,
  public previewImage:string,
	public releasedAt:string,
  public tags:Tag[]){
	}
}
