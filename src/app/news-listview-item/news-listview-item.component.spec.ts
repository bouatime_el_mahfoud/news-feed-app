import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsListviewItemComponent } from './news-listview-item.component';

describe('NewsListviewItemComponent', () => {
  let component: NewsListviewItemComponent;
  let fixture: ComponentFixture<NewsListviewItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsListviewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsListviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
