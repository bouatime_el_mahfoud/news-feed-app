import { Component, OnInit,Input } from '@angular/core';
import { Article } from '../models/article.model';
@Component({
  selector: 'app-news-listview-item',
  templateUrl: './news-listview-item.component.html',
  styleUrls: ['./news-listview-item.component.css']
})
export class NewsListviewItemComponent implements OnInit {
  @Input() article:Article;
  constructor() { }

  ngOnInit() {
  }

}
