import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ArticleService } from './services/article.service';
import { DomainService } from './services/domain.service';
import { HeaderComponent } from './header/header.component';
import { HeaderSearchbarComponent } from './header-searchbar/header-searchbar.component';
import { HeaderSelectButtonsComponent } from './header-select-buttons/header-select-buttons.component';
import { FooterComponent } from './footer/footer.component';
import { FooterArrowRightComponent } from './footer-arrow-right/footer-arrow-right.component';
import { FooterArrowLeftComponent } from './footer-arrow-left/footer-arrow-left.component';
import { FooterCountComponent } from './footer-count/footer-count.component';
import { ListNewsComponent } from './list-news/list-news.component';
import { NewsListviewItemComponent } from './news-listview-item/news-listview-item.component';
import { NewsCardviewItemComponent } from './news-cardview-item/news-cardview-item.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeaderSearchbarComponent,
    HeaderSelectButtonsComponent,
    FooterComponent,
    FooterArrowRightComponent,
    FooterArrowLeftComponent,
    FooterCountComponent,
    ListNewsComponent,
    NewsListviewItemComponent,
    NewsCardviewItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DomainService,ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
