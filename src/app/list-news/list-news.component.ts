import { Component, OnInit,OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ArticleService } from '../services/article.service';
import { Article } from '../models/article.model';
@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.css']
})
export class ListNewsComponent implements OnInit,OnDestroy {

  articlesSubscription:Subscription;
  listview:boolean;
  itemsCount:number;
  articles:Article[];
  remainingArticles:number;

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    //here we create a Subscription to listen to the articlesContent,count and isListView in the service ,so when those three elements change we get notified
    this.articlesSubscription=this.articleService.articlesContentSubject.subscribe(
      (articlesContent:any)=>{
        this.listview=articlesContent.isListView;
        this.itemsCount=articlesContent.ariclesCount;
        this.articles=articlesContent.articles;
        this.remainingArticles=1+6*(articlesContent.page-1);
      }
    );
    this.articleService.loadArticles(String(1));
  }

  ngOnDestroy(){
    //here we destroy our subscribtions
    this.articlesSubscription.unsubscribe();
  }
}
