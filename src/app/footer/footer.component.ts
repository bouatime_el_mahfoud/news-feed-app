import { Component, OnInit,OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ArticleService } from '../services/article.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit,OnDestroy {
  articlesSubscription:Subscription;
  totalPage:number;
  page:number;
  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    //here we create a Subscription to listen to the count and page in the service ,so when those two elements change we get notified
    this.articlesSubscription=this.articleService.articlesContentSubject.subscribe(
      (articlesContent:any)=>{
        this.totalPage=articlesContent.totalPages;
        this.page=articlesContent.page;
      }
    );
  }
  ngOnDestroy(){
    //here we destroy our subscribtions
    this.articlesSubscription.unsubscribe()
  }

}
