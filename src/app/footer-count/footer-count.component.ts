import { Component, OnInit,Input } from '@angular/core';
import { ArticleService } from '../services/article.service';
@Component({
  selector: 'app-footer-count',
  templateUrl: './footer-count.component.html',
  styleUrls: ['./footer-count.component.css']
})
export class FooterCountComponent implements OnInit {
  @Input() count:number;
  @Input() isSelected:boolean;
  constructor(private articaleService:ArticleService) { }

  ngOnInit() {
  }
  changePage(){
    this.articaleService.changePage(this.count)
  }

}
