import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSelectButtonsComponent } from './header-select-buttons.component';

describe('HeaderSelectButtonsComponent', () => {
  let component: HeaderSelectButtonsComponent;
  let fixture: ComponentFixture<HeaderSelectButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderSelectButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSelectButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
