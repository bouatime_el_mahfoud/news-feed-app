import { Component, OnInit ,OnDestroy} from '@angular/core';
import { ArticleService } from '../services/article.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-header-select-buttons',
  templateUrl: './header-select-buttons.component.html',
  styleUrls: ['./header-select-buttons.component.css']
})
export class HeaderSelectButtonsComponent implements OnInit {
  articlesSubscription:Subscription;
  isListView:boolean;
  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    //again we create a subscription to our article service to get notified when the isListView is changed
    this.articlesSubscription=this.articleService.articlesContentSubject.subscribe(
      (articlesContent:any)=>{

        this.isListView=articlesContent.isListView;
      }
    );
  }
  //using a method in article service we are capable to change the variable isListView wich is responsible of the way articles are displayed
  onChangeListView(isListView:boolean){
    this.articleService.changeListStyle(isListView)
  }
  ngOnDestroy(){
    //here we destroy our subscribtions,because we are destroying this component ,and we dont need to keep listnening to our article service
  	this.articlesSubscription.unsubscribe();


  }

}
