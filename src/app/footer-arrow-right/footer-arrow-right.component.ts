import { Component, OnInit,Input } from '@angular/core';
import { ArticleService } from '../services/article.service';
@Component({
  selector: 'app-footer-arrow-right',
  templateUrl: './footer-arrow-right.component.html',
  styleUrls: ['./footer-arrow-right.component.css']
})
export class FooterArrowRightComponent implements OnInit {
  @Input() isSelected:boolean;
  constructor(private articleService:ArticleService) { }

  ngOnInit() {
  }
  incrementPage(){
    this.articleService.incrementPage();
  }

}
