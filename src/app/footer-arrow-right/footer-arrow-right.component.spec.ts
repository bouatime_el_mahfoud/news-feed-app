import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterArrowRightComponent } from './footer-arrow-right.component';

describe('FooterArrowRightComponent', () => {
  let component: FooterArrowRightComponent;
  let fixture: ComponentFixture<FooterArrowRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterArrowRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterArrowRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
