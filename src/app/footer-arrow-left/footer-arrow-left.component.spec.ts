import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterArrowLeftComponent } from './footer-arrow-left.component';

describe('FooterArrowLeftComponent', () => {
  let component: FooterArrowLeftComponent;
  let fixture: ComponentFixture<FooterArrowLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterArrowLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterArrowLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
