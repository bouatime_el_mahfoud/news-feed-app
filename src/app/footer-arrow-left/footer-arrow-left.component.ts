import { Component, OnInit,Input } from '@angular/core';
import { ArticleService } from '../services/article.service';
@Component({
  selector: 'app-footer-arrow-left',
  templateUrl: './footer-arrow-left.component.html',
  styleUrls: ['./footer-arrow-left.component.css']
})
export class FooterArrowLeftComponent implements OnInit {
  @Input() isSelected:boolean;
  constructor(private articleService:ArticleService) { }

  ngOnInit() {
  }
  deincrementPage(){
    this.articleService.deincrementPage();
  }

}
