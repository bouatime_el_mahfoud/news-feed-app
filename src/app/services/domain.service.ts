import { Domain } from '../models/domain.model';
import { Subject } from 'rxjs';
export class DomainService{
	//this is the domain service ,it will take care of the buisness logic of the domains ,such as listing domains...
	//not implemented yet,it will be completed in the searchFeature branch.

	//global variables
	private domains:Domain[]=[
	{
    title:"hello"
	}
	];
	//a subject that allows to create a subscription so a component can recieve the latest changes of the global variables
	domainSubject=new Subject<Domain[]>();

	//send the latest value of the domain array to a component.
	emitDomainSubject(){
	this.domainSubject.next(this.domains.slice());
	}

}
