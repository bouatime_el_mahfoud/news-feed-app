import { Article } from '../models/article.model';
import { Tag } from '../models/tag.model';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class ArticleService{
  //articleService is a Class wich will take care of the buisness logic of our articles,this incluse search features ,loading articles..
  //no http communication is done yet.

  //those are the global variables we will use in our app
  private articlesContent={
    isListView:false,
    ariclesCount:0,
    page:0,
    totalPages:0,
    articles:[]
  }
  //those are the subject ,this is what will allows to create a Subscription in our components so we can listen to the changes of the global variables in this app,
	articlesContentSubject=new Subject<any>();
  constructor(private httpClient:HttpClient){

	}
  //sends the last value of the articles service variables
	emitArticleServiceSubject(){
	 this.articlesContentSubject.next(this.articlesContent);
  }
  //allows to change from the ListView to the cardView ,or the inverse
  changeListStyle(isListView:boolean){
    this.articlesContent.isListView=isListView;
    this.emitArticleServiceSubject();
  }

  //allows to change the page count to fetch a new page of articles
   changePage(pageCount:number){
     if(this.articlesContent.page!==pageCount){
      this.articlesContent.page=pageCount;
      this.loadArticles(String(pageCount));
    }
  }
  //allows to increment the page count to o fetch a new page of articles
   incrementPage(){
     this.articlesContent.page=this.articlesContent.page+1;
     this.loadArticles(String(this.articlesContent.page));
   }
  //allows to deincrement the page count to o fetch a new page of articles
   deincrementPage(){
     this.articlesContent.page=this.articlesContent.page-1;
     this.loadArticles(String(this.articlesContent.page));
  }
  loadArticles(page:string){
  console.log("heheh");
  this.articlesContent.articles=[];
	let headers = new HttpHeaders({'Accept':'application/json','x-algolia-key':'5949094591d487f83818c8de'});
  const formData=new FormData();
	formData.append('attributesToRetrieve',JSON.stringify(['name','domain','_tags','previewImage','releasedAt']));
	formData.append('page',page);
  formData.append('hitsPerPage',"6");
	this.httpClient.post('https://butler.luxurynsight.net/1/indexes/news/query',formData,{headers:headers}).subscribe(
	(response:any)=>{
		  response.hits.map((article)=>{
           var tags=[];
           article._tags.map((tag)=>{
             tags.push(new Tag(tag.id,tag.name,tag.counter,tag.visible));
           });
           var date=new Date(article.releasedAt);
           var dateToSave=date.getDate()+"/"+date.getMonth()+"/"+date.getFullYear();
           this.articlesContent.articles.push(new Article(article.objectId,article.name,article.domain,article.previewImage,dateToSave,tags));
      });
      this.articlesContent.page=response.page;
      this.articlesContent.ariclesCount=response.nbHits;
      this.articlesContent.totalPages=response.nbPages;
      this.emitArticleServiceSubject();

	},
    (error)=>{
    console.log("une erreur s'est produit :"+error);
    }
	);
	}


}
