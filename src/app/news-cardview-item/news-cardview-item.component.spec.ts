import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCardviewItemComponent } from './news-cardview-item.component';

describe('NewsCardviewItemComponent', () => {
  let component: NewsCardviewItemComponent;
  let fixture: ComponentFixture<NewsCardviewItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsCardviewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCardviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
