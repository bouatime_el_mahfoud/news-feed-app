import { Component, OnInit,Input } from '@angular/core';
import { Article } from '../models/article.model';
@Component({
  selector: 'app-news-cardview-item',
  templateUrl: './news-cardview-item.component.html',
  styleUrls: ['./news-cardview-item.component.css']
})
export class NewsCardviewItemComponent implements OnInit {
  @Input() article:Article;
  constructor() { }

  ngOnInit() {
  }

}
